import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FrontPageComponent } from './pages/front-page/front-page.component';
import { PlanetsPageComponent } from './pages/planets-page/planets-page.component';


const routes: Routes = [
  {
    path: 'front',
    component: FrontPageComponent
  },
  {
    path: 'planets',
    component: PlanetsPageComponent
  },
  {
    path: '',
    redirectTo: '/front',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

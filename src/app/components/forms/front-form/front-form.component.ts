import { Component, OnInit, TemplateRef, Inject } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { SESSION_STORAGE, WebStorageService } from 'angular-webstorage-service';
import { GlobalDataService } from '../../../globaldata.service';

@Component({
  selector: 'app-front-form',
  templateUrl: './front-form.component.html',
  styleUrls: ['./front-form.component.scss']
})
export class FrontFormComponent implements OnInit {

  key: string = null;
  modalRef: BsModalRef;
  constructor(private authService: AuthService,
    private modalService: BsModalService,
    private router: Router,
    @Inject(SESSION_STORAGE) private storage: WebStorageService,
    private gd: GlobalDataService) { }

  ngOnInit() {
    
  }

  onSubmit(): void {
    this.router.navigateByUrl('/planets');
  }

  generate_key(template: TemplateRef<any>) {

    this.authService.generateApiKey().subscribe((response) => {
      this.key = JSON.parse(response).apiKey;
      this.saveInSession('api_key', this.key);
      console.log("Saving api key" + this.key);
    });
    this.modalRef = this.modalService.show(template);
  }

  saveInSession(key, val): void {
    this.storage.set(key,val);
    this.key = this.storage.get(key);
    this.gd.shareObj[key] = this.key;
  }
}

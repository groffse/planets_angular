import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontModalComponent } from './front-modal.component';

describe('FrontModalComponent', () => {
  let component: FrontModalComponent;
  let fixture: ComponentFixture<FrontModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrontModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

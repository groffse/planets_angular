import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
  })

export class AuthService {

    httpOptions = {
        headers: new HttpHeaders({
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'bearer aee99f080aa708575451e4e347edec28b868cd0c6fca8beb089ac13b8c502eba'
        }
        ),
        
        responseType: 'text' as 'json'
      }

    private apiUrl = "https://desolate-badlands-17993.herokuapp.com/api/v1/auth/generate-key";
    private api_url_get_all = "https://desolate-badlands-17993.herokuapp.com/api/v1/planets";

    constructor (private http: HttpClient) { }

    generateApiKey(): Observable<any> {
        return this.http.post(this.apiUrl, '', this.httpOptions);
    }

    get_planets(): Observable<any> {
      return this.http.get(this.api_url_get_all, this.httpOptions);
    }
}


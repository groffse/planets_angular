import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})

export class PlanetsService {
    api_key: string = '';
    private api_url_get_all = "https://desolate-badlands-17993.herokuapp.com/api/v1/planets";
    private api_url_delete = "https://desolate-badlands-17993.herokuapp.com/api/v1/planets/delete/?planetId=";
    private api_url_update = "https://desolate-badlands-17993.herokuapp.com/api/v1/planets/update";
    private api_url_add = "https://desolate-badlands-17993.herokuapp.com/api/v1/planets/add";
    constructor(private http: HttpClient) { }

    httpOptions = {
        headers: new HttpHeaders({
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }
        ),
        responseType: 'text' as 'json'
      }

      // TODO: Test the api key. Return false if it does not exist in the 
      set_api_key(api_key) {
          this.api_key = api_key;
          this.httpOptions.headers = new HttpHeaders({
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            Authorization: this.api_key
          })
      }

      get_planets(): Observable<any> {
        return this.http.get(this.api_url_get_all, this.httpOptions);
      }

      delete_planet(id): any {
        return this.http.delete(this.api_url_delete + id, this.httpOptions);
      }

      update_planet(body) {
        return this.http.put(this.api_url_update, body, this.httpOptions);
      }

      add_planet(body) {
        return this.http.post(this.api_url_add, body, this.httpOptions);
      }
}
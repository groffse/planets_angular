import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FrontPageComponent } from './pages/front-page/front-page.component';
import { PlanetsPageComponent } from './pages/planets-page/planets-page.component';
import { FrontFormComponent } from './components/forms/front-form/front-form.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { BsDropdownModule} from 'ngx-bootstrap/dropdown';
import { FrontModalComponent } from './components/modals/front-modal/front-modal.component';
import { AuthService } from './components/services/auth/auth.service';
import { HttpClientModule } from '@angular/common/http';
import { PlanetsService } from './components/services/planets/planets.service';
import { StorageServiceModule } from 'angular-webstorage-service';
import { GlobalDataService } from './globaldata.service';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    FrontPageComponent,
    PlanetsPageComponent,
    FrontFormComponent,
    FrontModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ButtonsModule.forRoot(),
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    HttpClientModule,
    StorageServiceModule,
    FormsModule
  ],
  providers: [AuthService, PlanetsService, GlobalDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }

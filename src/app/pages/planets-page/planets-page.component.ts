import { Component, OnInit, TemplateRef } from '@angular/core';
import { PlanetsService } from '../../components/services/planets/planets.service'
import { FrontFormComponent } from 'src/app/components/forms/front-form/front-form.component';
import { GlobalDataService } from '../../globaldata.service';
import { HttpHandler, HttpClient } from '@angular/common/http';
import { Observable, generate } from 'rxjs';
import { Planet } from '../../models/planet.model';
import { Planet_Add } from '../../models/planet_add_model';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-planets-page',
  templateUrl: './planets-page.component.html',
  styleUrls: ['./planets-page.component.scss']
})
export class PlanetsPageComponent implements OnInit {

  planets: any [] = [];
  api_key : string = '';
  modalRef: BsModalRef;
  planet_model = {} as Planet;
  Planet_add_model = {} as Planet_Add;
  constructor(private gd: GlobalDataService,
    private modalService: BsModalService,
    private planetService: PlanetsService) { 
  }

  ngOnInit() {
    if(this.api_key = null) {
      alert("You haven't generated a key. Go back!");
    }
  }

  ngAfterViewInit () {
    this.api_key = this.gd.shareObj['api_key'];
    this.planetService.set_api_key("Bearer " + this.api_key);
    this.get_all_planets();
  }

  get_all_planets() {

    this.planetService.get_planets().subscribe((response ) => {
      this.planets = JSON.parse(response).data;
      console.log(this.planets);
    })
  }

  delete_planet(id) {
    console.log("delete planet with id" + id);
    this.planetService.delete_planet(id).subscribe(() => {
    });
  }

  // Entry to update
  onFormSubmit(planet_name) {

    let body = {
      "planet": {
        "id"              : this.planet_model.id,
        "name"            : planet_name,
        "size"            : this.planet_model.size,
        "image_url"       : this.planet_model.img_url,
        "date_discoverd" : this.planet_model.date_discovered
      }
    }

    this.planetService.update_planet(body).subscribe((response) => {
      console.log("Updated!");
    })
  }

  show_add_planet_modal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  add_planet() {
    let body = {
      "planet": {
        "name"            : this.Planet_add_model.name,
        "size"            : this.Planet_add_model.size,
        "image_url"       : this.Planet_add_model.img_url,
        "date_discovered" : this.Planet_add_model.date_discovered
      }
    }
    this.planetService.add_planet(body).subscribe((response) => {
      console.log("Added planet!");
    })
  }

}
export interface Planet_Add {
    id: number;
    name: string;
    size: string;
    date_discovered: string;
    img_url: string;
}
export interface Planet {
    id: number;
    name: string;
    size: string;
    date_discovered: string;
    img_url: string;
}